#include <igl/writeOFF.h>
#include <thread>
#include "Gui.h"
#include "Simulator.h"
#include "SPHSim.h"

/*
 * GUI for the SPH simulation.
 */
class SPHGui : public Gui {
public:
	double m_dt;
	double m_k;
	double m_mu;

	const vector<char const*> m_fields = {
	   "ID", "Density", "Velocity"
	};
	int m_selected_field;

	SPHSim* p_sphSim = NULL;

	SPHGui() {
		turnOffLight(); // no light for field visualization
		orthoCam(); // orthographic camera

		p_sphSim = new SPHSim();
		m_dt = p_sphSim->getTimestep();
		m_k = p_sphSim->getK();
		m_mu = p_sphSim->getMu();
		m_selected_field = p_sphSim->getField();
		setSimulation(p_sphSim);
		setFastForward(true); // no delay
		setParticleSystem(p_sphSim->getParticlesData());
		start();
	}

	virtual void updateSimulationParameters() override {
		// change all parameters of the simulation to the values that are set in
		// the GUI
		p_sphSim->setTimestep(m_dt);
		p_sphSim->setK(m_k);
		p_sphSim->setMu(m_mu);
	}

	virtual void drawSimulationParameterMenu() override {
		p_sphSim->updateRenderGeometry();
		if (ImGui::Combo("Fields", &m_selected_field,
			m_fields.data(), m_fields.size())) {
			p_sphSim->selectField(m_selected_field);
			p_sphSim->updateRenderGeometry();
		}
		ImGui::InputDouble("dt", &m_dt, 0, 0);
		ImGui::InputDouble("k", &m_k, 0, 0);
		ImGui::InputDouble("mu", &m_mu, 0, 0);
	}
};

int main(int argc, char* argv[]) {
	// create a new instance of the GUI for the SPH simulation
	new SPHGui();

	return 0;
}