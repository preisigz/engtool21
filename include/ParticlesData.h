//  Copyright (c) 2013, Vinicius Costa Azevedo
//	All rights reserved.
//
//	Redistribution and use in source and binary forms, with or without
//	modification, are permitted provided that the following conditions are met: 
//
//1. Redistributions of source code must retain the above copyright notice, this
//	list of conditions and the following disclaimer. 
//	2. Redistributions in binary form must reproduce the above copyright notice,
//	this list of conditions and the following disclaimer in the documentation
//	and/or other materials provided with the distribution. 
//
//	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//	DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
//	ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//	The views and conclusions contained in the software and documentation are those
//	of the authors and should not be interpreted as representing official policies, 
//	either expressed or implied, of the FreeBSD Project.

#ifndef PARTICLES_DATA_H_
#define PARTICLES_DATA_H_
#pragma once

#include "OwnCustomAttribute.h"
#include <Eigen/Core>

using namespace std;


class ParticlesData : public OwnCustomAttribute<vector<Eigen::Vector3d>>, public OwnCustomAttribute <vector<float>>,
	public OwnCustomAttribute<vector<int>> {

public:
	//Will only reserve sizes inside vectors, explicit position/velocity initialization is on user-side
	ParticlesData(int initialNumParticles) {
		m_numInitialParticles = initialNumParticles;
		m_positions.reserve(initialNumParticles);
		m_velocities.reserve(initialNumParticles);
	}

	#pragma region Acceess Functions
	/** Simple "getters and setters"*/
	int getNumParticles() const {
		return m_positions.size();
	}

	const vector<Eigen::Vector3d>& getPositions() const {
		return m_positions;
	}

	vector<Eigen::Vector3d>& getPositions() {
		return m_positions;
	}

	const vector<Eigen::Vector3d>& getVelocities() const {
		return m_velocities;
	}

	vector<Eigen::Vector3d>& getVelocities() {
		return m_velocities;
	}

	const vector<bool>& getResampledParticles() const {
		return m_resampledParticles;
	}

	vector<bool>& getResampledParticles() {
		return m_resampledParticles;
	}

	/** Adds a particle: initializes the position and adds placeholders for all custom allocated variables */
	void addParticle(const Eigen::Vector3d& position) {
		m_positions.push_back(position);
		m_velocities.push_back(Eigen::Vector3d());
		//This is a particle that is initally sampled
		m_resampledParticles.push_back(true);


		auto vectorBasedAttributes = OwnCustomAttribute<vector<Eigen::Vector3d>>::get(this)->getAttributesMap();
		for (auto iter = vectorBasedAttributes.begin(); iter != vectorBasedAttributes.end(); iter++) {
			iter->second.push_back(Eigen::Vector3d());
		}

		auto scalarBasedAttributes = OwnCustomAttribute<vector<float>>::get(this)->getAttributesMap();
		for (auto iter = scalarBasedAttributes.begin(); iter != scalarBasedAttributes.end(); iter++) {
			iter->second.push_back(0.0f);
		}

		auto intBasedAttributes = OwnCustomAttribute<vector<int>>::get(this)->getAttributesMap();
		for (auto iter = intBasedAttributes.begin(); iter != intBasedAttributes.end(); iter++) {
			iter->second.push_back(0);
		}
	}

	/** Use this function for resampling, since it resets all interior particle fields */
	void resampleParticle(uint particleIndex, const Eigen::Vector3d& position) {
		m_positions[particleIndex] = position;
		Eigen::Vector3d zeroVelocity;
		//Zero out velocity
		m_velocities[particleIndex] = zeroVelocity;

		auto vectorBasedAttributes = OwnCustomAttribute<vector<Eigen::Vector3d>>::get(this)->getAttributesMap();
		for (auto iter = vectorBasedAttributes.begin(); iter != vectorBasedAttributes.end(); iter++) {
			iter->second[particleIndex] = zeroVelocity;
		}

		auto scalarBasedAttributes = OwnCustomAttribute<vector<float>>::get(this)->getAttributesMap();
		for (auto iter = scalarBasedAttributes.begin(); iter != scalarBasedAttributes.end(); iter++) {
			iter->second[particleIndex] = 0.f;
		}

		auto intBasedAttributes = OwnCustomAttribute<vector<int>>::get(this)->getAttributesMap();
		for (auto iter = intBasedAttributes.begin(); iter != intBasedAttributes.end(); iter++) {
			iter->second[particleIndex] = 0;
		}

		m_resampledParticles[particleIndex] = true;
	}

	#pragma endregion
	protected:
		int m_numInitialParticles;
		/* Standard particles properties*/
		vector<Eigen::Vector3d> m_positions;
		vector<Eigen::Vector3d> m_velocities;
		/** Is important to tag if the particles were resampled, for both rendering & simulation purposes */
		vector<bool> m_resampledParticles;
};

#endif
